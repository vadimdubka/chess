package com.vdubka.chess.service;


import com.vdubka.chess.domain.Point;

public interface StepInputParsingService {
    boolean isStepInputCorrect(String userStepInput);
    
    Point getFromPoint(String userStepInput);
    
    Point getToPoint(String userStepInput);
    
    Point parse(String userStepInput);
}
