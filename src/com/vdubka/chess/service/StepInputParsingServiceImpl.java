package com.vdubka.chess.service;

import com.vdubka.chess.domain.Point;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StepInputParsingServiceImpl implements StepInputParsingService {
    private static final Pattern STEP_PATTERN = Pattern.compile("([a-hA-H][1-8])([-])([a-hA-H][1-8])",
                                                                Pattern.CASE_INSENSITIVE);
    
    @Override
    public boolean isStepInputCorrect(String userStepInput) {
        return STEP_PATTERN.matcher(userStepInput).matches();
    }
    
    @Override
    public Point getFromPoint(String userStepInput) {
        Matcher matcher = STEP_PATTERN.matcher(userStepInput);
        matcher.matches();
        String coords = matcher.group(1);
        return parse(coords);
    }
    
    @Override
    public Point getToPoint(String userStepInput) {
        Matcher matcher = STEP_PATTERN.matcher(userStepInput);
        matcher.matches();
        String coords = matcher.group(3);
        return parse(coords);
    }
    
    @Override
    public Point parse(String userStepInput) {
        int x = convertColumnValueToMatrixIndex(userStepInput.charAt(0));
        int y = convertRowNumberToMatrixIndex(Integer.parseInt(String.valueOf(userStepInput.charAt(1))));
        return new Point(x, y);
    }
    
    private static int convertRowNumberToMatrixIndex(int rowNumber) {
        return 8 - rowNumber;
    }
    
    private static int convertColumnValueToMatrixIndex(char column) {
        switch (Character.toLowerCase(column)) {
            case 'a':
                return 0;
            case 'b':
                return 1;
            case 'c':
                return 2;
            case 'd':
                return 3;
            case 'e':
                return 4;
            case 'f':
                return 5;
            case 'g':
                return 6;
            case 'h':
                return 7;
            default:
                return -1;
        }
    }
}
