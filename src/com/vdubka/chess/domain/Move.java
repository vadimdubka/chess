package com.vdubka.chess.domain;

public class Move {
    private final int x;
    private final int y;
    private final boolean onFirstMoveOnly;
    private final boolean onCaptureOnly;
    
    public Move(int x, int y, boolean onFirstMoveOnly, boolean onCaptureOnly) {
        this.x = x;
        this.y = y;
        this.onFirstMoveOnly = onFirstMoveOnly;
        this.onCaptureOnly = onCaptureOnly;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    public boolean isOnFirstMoveOnly() {
        return onFirstMoveOnly;
    }
    
    public boolean isOnCaptureOnly() {
        return onCaptureOnly;
    }
}
