package com.vdubka.chess.domain;


import com.vdubka.chess.strategy.StepCorrectnessStrategy;

public class Piece {
    private final MoveType moveType;
    private final StepCorrectnessStrategy stepCorrectnessStrategy;
    private final PieceColor pieceColor;
    private final char charValue;
    
    public Piece(MoveType moveType, StepCorrectnessStrategy stepCorrectnessStrategy, PieceColor pieceColor) {
        this.moveType = moveType;
        this.stepCorrectnessStrategy = stepCorrectnessStrategy;
        this.pieceColor = pieceColor;
        this.charValue = (PieceColor.WHITE == pieceColor)
                             ? Character.toLowerCase(moveType.name().charAt(0))
                             : Character.toUpperCase(moveType.name().charAt(0));
    }
    
    public Move[] getMoves() {
        return moveType.getMoves();
    }
    
    public PieceColor getPieceColor() {
        return pieceColor;
    }
    
    public char getCharValue() {
        return charValue;
    }
    
    public boolean hasRepeatableMoves() {
        return moveType.hasRepeatableMoves();
    }
    
    public MoveType getMoveType() {
        return moveType;
    }
    
    public StepCorrectnessStrategy getStepCorrectnessStrategy() {
        return stepCorrectnessStrategy;
    }
    
}
