package com.vdubka.chess.domain;

public class Cell {
    private Piece piece;
    
    public void setPiece(Piece piece) {
        this.piece = piece;
    }
    
    public Piece getPiece() {
        return piece;
    }
    
    public boolean isEmpty() {
        return piece == null;
    }
    
    public void setEmptyPiece() {
        piece = null;
    }
}
