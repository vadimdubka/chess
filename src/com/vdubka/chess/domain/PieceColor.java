package com.vdubka.chess.domain;

public enum PieceColor {
    WHITE, BLACK
}