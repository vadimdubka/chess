package com.vdubka.chess.domain;

import com.vdubka.chess.strategy.StepCorrectnessStrategyFactory;

import java.util.ArrayList;

public class Field {
    private final Cell[][] matrix = new Cell[8][8];
    
    public Field() {
        initMatrix();
        placePieces();
    }
    
    public Cell getCellForPoint(Point point) {
        return matrix[point.getY()][point.getX()];
    }
    
    public String getCellValue(int y, int x) {
        Cell cell = matrix[y][x];
        return cell.isEmpty()
                   ? "[ ]"
                   : String.format("[%s]", cell.getPiece().getCharValue());
    }
    
    public Point getKingPointForColor(PieceColor color) {
        Point location = null;
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (!matrix[y][x].isEmpty()) {
                    Piece piece = matrix[y][x].getPiece();
                    if ((piece.getMoveType() == MoveType.KING) && (piece.getPieceColor() == color)) {
                        return new Point(x, y);
                    }
                }
            }
        }
        return location;
    }
    
    public Point[] getAllPiecesPointsForColor(PieceColor color) {
        ArrayList<Point> locations = new ArrayList<>();
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (!matrix[y][x].isEmpty() && (matrix[y][x].getPiece().getPieceColor() == color)) {
                    locations.add(new Point(x, y));
                }
            }
        }
        return locations.toArray(new Point[0]);
    }
    
    private void initMatrix() {
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                matrix[x][y] = new Cell();
            }
        }
    }
    
    private void placePieces() {
        for (int i = 0; i < 8; i++) {
            matrix[1][i].setPiece(new Piece(MoveType.PAWN_BLACK, StepCorrectnessStrategyFactory.getNonRepeatableStepCorrectnessStrategy(), PieceColor.BLACK));
            matrix[6][i].setPiece(new Piece(MoveType.PAWN_WHITE, StepCorrectnessStrategyFactory.getNonRepeatableStepCorrectnessStrategy(), PieceColor.WHITE));
        }
        matrix[0][1].setPiece(new Piece(MoveType.KNIGHT, StepCorrectnessStrategyFactory.getNonRepeatableStepCorrectnessStrategy(), PieceColor.BLACK));
        matrix[0][6].setPiece(new Piece(MoveType.KNIGHT, StepCorrectnessStrategyFactory.getNonRepeatableStepCorrectnessStrategy(), PieceColor.BLACK));
        matrix[7][1].setPiece(new Piece(MoveType.KNIGHT, StepCorrectnessStrategyFactory.getNonRepeatableStepCorrectnessStrategy(), PieceColor.WHITE));
        matrix[7][6].setPiece(new Piece(MoveType.KNIGHT, StepCorrectnessStrategyFactory.getNonRepeatableStepCorrectnessStrategy(), PieceColor.WHITE));
        
        matrix[0][4].setPiece(new Piece(MoveType.KING, StepCorrectnessStrategyFactory.getNonRepeatableStepCorrectnessStrategy(), PieceColor.BLACK));
        matrix[7][4].setPiece(new Piece(MoveType.KING, StepCorrectnessStrategyFactory.getNonRepeatableStepCorrectnessStrategy(), PieceColor.WHITE));
        
        matrix[0][0].setPiece(new Piece(MoveType.ROOK, StepCorrectnessStrategyFactory.getRepeatableStepCorrectnessStrategy(), PieceColor.BLACK));
        matrix[0][7].setPiece(new Piece(MoveType.ROOK, StepCorrectnessStrategyFactory.getRepeatableStepCorrectnessStrategy(), PieceColor.BLACK));
        matrix[7][0].setPiece(new Piece(MoveType.ROOK, StepCorrectnessStrategyFactory.getRepeatableStepCorrectnessStrategy(), PieceColor.WHITE));
        matrix[7][7].setPiece(new Piece(MoveType.ROOK, StepCorrectnessStrategyFactory.getRepeatableStepCorrectnessStrategy(), PieceColor.WHITE));
        
        matrix[0][2].setPiece(new Piece(MoveType.BISHOP, StepCorrectnessStrategyFactory.getRepeatableStepCorrectnessStrategy(), PieceColor.BLACK));
        matrix[0][5].setPiece(new Piece(MoveType.BISHOP, StepCorrectnessStrategyFactory.getRepeatableStepCorrectnessStrategy(), PieceColor.BLACK));
        matrix[7][2].setPiece(new Piece(MoveType.BISHOP, StepCorrectnessStrategyFactory.getRepeatableStepCorrectnessStrategy(), PieceColor.WHITE));
        matrix[7][5].setPiece(new Piece(MoveType.BISHOP, StepCorrectnessStrategyFactory.getRepeatableStepCorrectnessStrategy(), PieceColor.WHITE));
        
        matrix[0][3].setPiece(new Piece(MoveType.QUEEN, StepCorrectnessStrategyFactory.getRepeatableStepCorrectnessStrategy(), PieceColor.BLACK));
        matrix[7][3].setPiece(new Piece(MoveType.QUEEN, StepCorrectnessStrategyFactory.getRepeatableStepCorrectnessStrategy(), PieceColor.WHITE));
    }
}
