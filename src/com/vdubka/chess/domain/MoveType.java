package com.vdubka.chess.domain;

public enum MoveType {
    PAWN_WHITE(false, new Move[]{
        new Move(0, -1, false, false),
        new Move(0, -2, true, false),
        new Move(1, -1, false, true),
        new Move(-1, -1, false, true)}),
    PAWN_BLACK(false, new Move[]{
        new Move(0, 1, false, false),
        new Move(0, 2, true, false),
        new Move(1, 1, false, true),
        new Move(-1, 1, false, true)}),
    KNIGHT(false, new Move[]{
        new Move(2, 1, false, false),
        new Move(1, 2, false, false),
        new Move(2, -1, false, false),
        new Move(-1, 2, false, false),
        new Move(2, -1, false, false),
        new Move(-1, 2, false, false),
        new Move(-2, 1, false, false),
        new Move(1, -2, false, false),
        new Move(-2, -1, false, false),
        new Move(-1, -2, false, false),
        new Move(-2, -1, false, false),
        new Move(-1, -2, false, false)}),
    KING(false, new Move[]{
        new Move(1, 0, false, false),
        new Move(0, 1, false, false),
        new Move(-1, 0, false, false),
        new Move(0, -1, false, false),
        new Move(1, 1, false, false),
        new Move(1, -1, false, false),
        new Move(-1, 1, false, false),
        new Move(-1, -1, false, false)}),
    ROOK(true, new Move[]{
        new Move(1, 0, false, false),
        new Move(0, 1, false, false),
        new Move(-1, 0, false, false),
        new Move(0, -1, false, false)}),
    BISHOP(true, new Move[]{
        new Move(1, 1, false, false),
        new Move(1, -1, false, false),
        new Move(-1, 1, false, false),
        new Move(-1, -1, false, false)}),
    QUEEN(true, new Move[]{
        new Move(1, 0, false, false),
        new Move(0, 1, false, false),
        new Move(-1, 0, false, false),
        new Move(0, -1, false, false),
        new Move(1, 1, false, false),
        new Move(1, -1, false, false),
        new Move(-1, 1, false, false),
        new Move(-1, -1, false, false)});
    
    private final Move[] moves;
    private final boolean repeatableMoves;
    
    MoveType(boolean repeatableMoves, Move[] moves) {
        this.moves = moves;
        this.repeatableMoves = repeatableMoves;
    }
    
    public Move[] getMoves() {
        return moves;
    }
    
    public boolean hasRepeatableMoves() {
        return repeatableMoves;
    }
}