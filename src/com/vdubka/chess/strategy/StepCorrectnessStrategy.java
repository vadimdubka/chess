package com.vdubka.chess.strategy;

import com.vdubka.chess.domain.Field;
import com.vdubka.chess.domain.PieceColor;
import com.vdubka.chess.domain.Point;

public interface StepCorrectnessStrategy {
    boolean isStepCorrectForPiece(Field field, Point fromPoint, Point toPoint, PieceColor currentPlayerColor);
}
