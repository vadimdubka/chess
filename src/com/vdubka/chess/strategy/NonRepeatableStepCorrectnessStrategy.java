package com.vdubka.chess.strategy;

import com.vdubka.chess.domain.Cell;
import com.vdubka.chess.domain.Field;
import com.vdubka.chess.domain.Move;
import com.vdubka.chess.domain.MoveType;
import com.vdubka.chess.domain.Piece;
import com.vdubka.chess.domain.PieceColor;
import com.vdubka.chess.domain.Point;

public class NonRepeatableStepCorrectnessStrategy implements StepCorrectnessStrategy {
    @Override
    public boolean isStepCorrectForPiece(Field field, Point fromPoint, Point toPoint, PieceColor currentPlayerColor) {
        Piece fromPiece = field.getCellForPoint(fromPoint).getPiece();
        Move[] moves = fromPiece.getMoves();
        Cell toCell = field.getCellForPoint(toPoint);
        
        int xMove = toPoint.getX() - fromPoint.getX();
        int yMove = toPoint.getY() - fromPoint.getY();
        
        for (Move move : moves) {
            if ((move.getX() == xMove) && (move.getY() == yMove)) {
                if (move.isOnCaptureOnly()) {//for pawns
                    return !toCell.isEmpty() && (fromPiece.getPieceColor() != toCell.getPiece().getPieceColor());
                } else if (move.isOnFirstMoveOnly()) { //for pawns
                    return toCell.isEmpty() && isFirstMoveForPawn(fromPoint, field);
                } else {
                    return toCell.isEmpty() || (fromPiece.getPieceColor() != toCell.getPiece().getPieceColor());
                }
            }
        }
        return false;
    }
    
    private boolean isFirstMoveForPawn(Point from, Field field) {
        Cell cell = field.getCellForPoint(from);
        if (cell.isEmpty()
                || ((cell.getPiece().getMoveType() != MoveType.PAWN_WHITE)
                        && (cell.getPiece().getMoveType() != MoveType.PAWN_BLACK))) {
            return false;
        } else {
            PieceColor color = cell.getPiece().getPieceColor();
            return (color == PieceColor.WHITE)
                       ? (from.getY() == 6)
                       : (from.getY() == 1);
        }
    }
}
