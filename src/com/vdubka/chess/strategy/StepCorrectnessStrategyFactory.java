package com.vdubka.chess.strategy;

public class StepCorrectnessStrategyFactory {
    private static final StepCorrectnessStrategy REPEATABLE_STEP_CORRECTNESS_STRATEGY = new RepeatableStepCorrectnessStrategy();
    private static final StepCorrectnessStrategy NON_REPEATABLE_STEP_CORRECTNESS_STRATEGY = new NonRepeatableStepCorrectnessStrategy();
    
    private StepCorrectnessStrategyFactory() {
    }
    
    public static StepCorrectnessStrategy getNonRepeatableStepCorrectnessStrategy() {
        return NON_REPEATABLE_STEP_CORRECTNESS_STRATEGY;
    }
    
    public static StepCorrectnessStrategy getRepeatableStepCorrectnessStrategy() {
        return REPEATABLE_STEP_CORRECTNESS_STRATEGY;
    }
    
}
