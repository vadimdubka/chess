package com.vdubka.chess.strategy;

import com.vdubka.chess.domain.Cell;
import com.vdubka.chess.domain.Field;
import com.vdubka.chess.domain.Move;
import com.vdubka.chess.domain.Piece;
import com.vdubka.chess.domain.PieceColor;
import com.vdubka.chess.domain.Point;

public class RepeatableStepCorrectnessStrategy implements StepCorrectnessStrategy {
    
    @Override
    public boolean isStepCorrectForPiece(Field field, Point fromPoint, Point toPoint, PieceColor currentPlayerColor) {
        Piece fromPiece = field.getCellForPoint(fromPoint).getPiece();
        Move[] moves = fromPiece.getMoves();
        int xMove = toPoint.getX() - fromPoint.getX();
        int yMove = toPoint.getY() - fromPoint.getY();
        
        for (Move move : moves) {
            for (int i = 1; i <= 7; i++) {
                // define available move
                if (((move.getX() * i) == xMove) && ((move.getY() * i) == yMove)) {
                    //check path
                    for (int j = 1; j <= i; j++) {
                        Point point = new Point(fromPoint.getX() + (move.getX() * j),
                                                fromPoint.getY() + (move.getY() * j));
                        Cell cell = field.getCellForPoint(point);
                        boolean isLastMove = j == i;
                        if (isLastMove) {
                            if (cell.isEmpty() || (cell.getPiece().getPieceColor() != currentPlayerColor)) {
                                return true;
                            }
                        } else {
                            if (!cell.isEmpty()) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}
