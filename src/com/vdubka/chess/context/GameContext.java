package com.vdubka.chess.context;

import com.vdubka.chess.domain.Cell;
import com.vdubka.chess.domain.Field;
import com.vdubka.chess.domain.Move;
import com.vdubka.chess.domain.Piece;
import com.vdubka.chess.domain.PieceColor;
import com.vdubka.chess.domain.Point;
import com.vdubka.chess.service.StepInputParsingService;
import com.vdubka.chess.service.StepInputParsingServiceImpl;
import com.vdubka.chess.strategy.StepCorrectnessStrategy;

import java.util.ArrayList;
import java.util.Scanner;

public class GameContext {
    private static final Point[] DUMMY_POINT_ARRAY = new Point[0];
    private final StepInputParsingService parsingService = new StepInputParsingServiceImpl();
    private final Field field = new Field();
    
    private PieceColor currentPlayerColor = PieceColor.WHITE;
    private boolean isGameFinished = false;
    
    public void startGame() {
        printField();
        System.out.println("Correct step input example is A2-A3");
        try (Scanner scanner = new Scanner(System.in)) {
            while (!isGameFinished) {
                System.out.print(currentPlayerColor + " step: ");
                String stepInput = scanner.nextLine();
                if (parsingService.isStepInputCorrect(stepInput)) {
                    Point fromPoint = parsingService.getFromPoint(stepInput);
                    Point toPoint = parsingService.getToPoint(stepInput);
                    makeStep(fromPoint, toPoint);
                } else {
                    System.err.println("Incorrect step input form!");
                }
            }
        }
        System.out.printf("Game completed. %s win!", getOppositeColor(currentPlayerColor));
    }
    
    private void makeStep(Point fromPoint, Point toPoint) {
        Cell fromCell = field.getCellForPoint(fromPoint);
        Cell toCell = field.getCellForPoint(toPoint);
        Piece fromPiece = fromCell.getPiece();
        Piece toPiece = toCell.getPiece();
        
        if (isStepAvailable(fromPiece, toPiece)
            && isStepCorrectForPieceMoveType(fromPoint, toPoint)
            && !isCurrentPlayerKingUnderCheck(fromCell, toCell)) {
            toCell.setPiece(fromPiece);
            fromCell.setEmptyPiece();
            
            isGameFinished = isCheckMateForOpponent();
            switchCurrentPlayerColor();
            printField();
        } else {
            System.err.println("Unacceptable step!");
        }
    }
    
    private void switchCurrentPlayerColor() {
        currentPlayerColor = (currentPlayerColor == PieceColor.WHITE)
                                 ? PieceColor.BLACK
                                 : PieceColor.WHITE;
    }
    
    private void printField() {
        System.out.println();
        System.out.println("   |A||B||C||D||E||F||G||H| \n");
        for (int y = 0; y < 8; y++) {
            System.out.print((8 - y) + "| ");
            
            for (int x = 0; x < 8; x++) {
                System.out.print(field.getCellValue(y, x));
            }
            
            System.out.println(" |" + (8 - y));
        }
        
        System.out.println("\n   |A||B||C||D||E||F||G||H|\n");
    }
    
    private boolean isStepAvailable(Piece fromPiece, Piece toPiece) {
        return (fromPiece != null)
               && (currentPlayerColor == fromPiece.getPieceColor())
               && ((toPiece == null) || (toPiece.getPieceColor() != currentPlayerColor));
    }
    
    private boolean isStepCorrectForPieceMoveType(Point fromPoint, Point toPoint) {
        Piece fromPiece = field.getCellForPoint(fromPoint).getPiece();
        StepCorrectnessStrategy strategy = fromPiece.getStepCorrectnessStrategy();
        return strategy.isStepCorrectForPiece(field, fromPoint, toPoint, currentPlayerColor);
    }
    
    private boolean isCurrentPlayerKingUnderCheck(Cell fromCell, Cell toCell) {
        Piece fromPiece = fromCell.getPiece();
        Piece toPiece = toCell.getPiece();
        
        // temporal step
        toCell.setPiece(fromPiece);
        fromCell.setEmptyPiece();
        
        boolean isCurrentPlayerKingUnderCheck = isKingCheck(currentPlayerColor);
        if (isCurrentPlayerKingUnderCheck) {
            System.out.println(currentPlayerColor + " King is under check!");
        }
        
        //revert step
        toCell.setPiece(toPiece);
        fromCell.setPiece(fromPiece);
        return isCurrentPlayerKingUnderCheck;
    }
    
    private boolean isKingCheck(PieceColor kingColor) {
        Point kingPoint = field.getKingPointForColor(kingColor);
        return canOpponentTakeLocation(kingPoint, kingColor);
    }
    
    private boolean canOpponentTakeLocation(Point kingPoint, PieceColor kingColor) {
        PieceColor opponentColor = getOppositeColor(kingColor);
        Piece kingPointPiece = field.getCellForPoint(kingPoint).getPiece();
        Point[] points = field.getAllPiecesPointsForColor(opponentColor);
        for (Point fromPoint : points) {
            Piece fromPiece = field.getCellForPoint(fromPoint).getPiece();
            if (isStepAvailable(fromPiece, kingPointPiece) && isStepCorrectForPieceMoveType(fromPoint, kingPoint)) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isCheckMateForOpponent() {
        PieceColor opponentColor = getOppositeColor(currentPlayerColor);
        return isKingCheck(opponentColor) && !isCheckPreventable(opponentColor);
    }
    
    private boolean isCheckPreventable(PieceColor color) {
        boolean canPreventCheck = false;
        Point[] points = field.getAllPiecesPointsForColor(color);
        
        for (Point fromPoint : points) {
            Cell fromCell = field.getCellForPoint(fromPoint);
            Piece piece = fromCell.getPiece();
            Point[] possibleMoves = correctStepsForPiece(piece, fromPoint);
            
            for (Point newLocation : possibleMoves) {
                Cell toCell = field.getCellForPoint(newLocation);
                Piece toPiece = toCell.getPiece();
                
                toCell.setPiece(piece);
                fromCell.setEmptyPiece();
                
                if (!isKingCheck(color)) {
                    canPreventCheck = true;
                }
                
                toCell.setPiece(toPiece);
                fromCell.setPiece(piece);
                if (canPreventCheck) {
                    return canPreventCheck;
                }
            }
        }
        return canPreventCheck;
    }
    
    private Point[] correctStepsForPiece(Piece piece, Point fromPoint) {
        return piece.hasRepeatableMoves()
                   ? correctStepsRepeatable(piece, fromPoint)
                   : correctStepsNonRepeatable(piece, fromPoint);
    }
    
    private Point[] correctStepsRepeatable(Piece piece, Point fromPoint) {
        Move[] moves = piece.getMoves();
        ArrayList<Point> possibleMoves = new ArrayList<>();
        for (Move move : moves) {
            for (int i = 1; i < 7; i++) {
                int newX = fromPoint.getX() + (move.getX() * i);
                int newY = fromPoint.getY() + (move.getY() * i);
                if ((newX < 0) || (newX > 7) || (newY < 0) || (newY > 7)) {
                    break;
                }
                
                Point toLocation = new Point(newX, newY);
                Cell cell = field.getCellForPoint(toLocation);
                if (cell.isEmpty()) {
                    possibleMoves.add(toLocation);
                } else {
                    if (cell.getPiece().getPieceColor() != piece.getPieceColor()) {
                        possibleMoves.add(toLocation);
                    }
                    break;
                }
            }
        }
        return possibleMoves.toArray(DUMMY_POINT_ARRAY);
    }
    
    private Point[] correctStepsNonRepeatable(Piece piece, Point fromPoint) {
        Move[] moves = piece.getMoves();
        ArrayList<Point> possibleMoves = new ArrayList<>();
        for (Move move : moves) {
            int currentX = fromPoint.getX();
            int currentY = fromPoint.getY();
            int newX = currentX + move.getX();
            int newY = currentY + move.getY();
            if ((newX < 0) || (newX > 7) || (newY < 0) || (newY > 7)) {
                continue;
            }
            Point newLocation = new Point(newX, newY);
            if (isStepCorrectForPieceMoveType(fromPoint, newLocation)) {
                possibleMoves.add(newLocation);
            }
        }
        return possibleMoves.toArray(DUMMY_POINT_ARRAY);
    }
    
    private static PieceColor getOppositeColor(PieceColor color) {
        return (color == PieceColor.BLACK) ? PieceColor.WHITE : PieceColor.BLACK;
    }
    
}
