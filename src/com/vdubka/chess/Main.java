package com.vdubka.chess;

import com.vdubka.chess.context.GameContext;

public class Main {
    
    public static void main(String args[]) {
        new GameContext().startGame();
    }
}
